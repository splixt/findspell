﻿if ($args.Count -ne 1) {
    Write-Information -InformationAction Stop "Usage: findspell <class abbrev>"
}

$class_abbrev = $args[0]

$eq_path = "C:\Users\Public\Daybreak Game Company\Installed Games\Everquest"
$spells_us_path = $eq_path + "\spells_us.txt"
$dbstr_us_path = $eq_path + "\dbstr_us.txt"

# spell_us field indexes  (names are from Lucy.allakhazam.com)

switch ($class_abbrev) {
    "clr" {
        $class_level = 37
    }
    "pal" {
        $class_level = 38
    }
    "rng" {
        $class_level = 39
    }
    "shd" {
        $class_level = 40
    }
    "dru" {
        $class_level = 41
    }
    "brd" {
        $class_level = 43
    }
    "shm" {
        $class_level = 45
    }
    "nec" {
        $class_level = 46
    }
    "wiz" {
        $class_level = 47
    }
    "enc" {
        $class_level = 49
    }
    "bst" {
        $class_level = 50
    }
    "mag" {
        $class_level = 48
    }
    default {
        Write-Information -InformationAction Stop "Unrecognized class abbreviation: $class_abbrev"
    }
}
$sp_name = 1
$sp_menu1 = 86
$sp_menu2 = 87
$sp_menu3 = 88

Write-Information -InformationAction Continue "Reading $spells_us_path ..."

$spells_us = Get-Content -Path $spells_us_path

Write-Information -InformationAction Continue "Reading $dbstr_us_path ..."

$dbstr_us = Get-Content -Path $dbstr_us_path

Write-Information -InformationAction Continue "Parsing spell data ..."

# turn the files into arrays of arrays
$spells_us_arr = @()
foreach ($line in $spells_us) {
    $fields = $line.Split("^")
    if ([int]($fields[$class_level]) -ne 255) {
        $spells_us_arr += ,($fields[$sp_name], [int]$fields[$class_level], [int]$fields[$sp_menu1], [int]$fields[$sp_menu2], [int]$fields[$sp_menu3])
    }
    # Write-Information -InformationAction Continue ($fields[$sp_name] + ":" + $fields[$class_level])
}

Write-Information -InformationAction Continue "Parsing dbstr data ..."

# dbstr_us field indexes
$db_idx = 0
$db_subcategory = 1
$db_string = 2

$match_subcategory = 5

$dbstr_us_hash = @{}
foreach ($line in $dbstr_us) {
    $fields = $line.Split("^")
    if ([int]($fields[$db_subcategory]) -eq $match_subcategory) {
        $dbstr_us_hash[[int]$fields[$db_idx]] = $fields[$db_string]
    }
}

while ($True) {
    Write-Information -InformationAction Continue ""
    $spell_regex = Read-Host -Prompt "Spell regex"
    $found = $False
    foreach ($spell in $spells_us_arr) {
        if ($spell[0] -imatch $spell_regex) {
            $found = $True
            Write-Information -InformationAction Continue ("Found level " + [string]$spell[1] + " spell: " + $spell[0])
            Write-Information -InformationAction Continue ("    at  " + $dbstr_us_hash[$spell[2]] + " -> " + $dbstr_us_hash[$spell[3]])
            if ($spell[4] -ne 0) {
               Write-Information -InformationAction Continue ("    and " + $dbstr_us_hash[$spell[2]] + " -> " + $dbstr_us_hash[$spell[4]])
            }
        }
    }
    if (! $found) {
        Write-Information -InformationAction Continue ("No matching spells found.  Please double check your spelling.")
    }
}